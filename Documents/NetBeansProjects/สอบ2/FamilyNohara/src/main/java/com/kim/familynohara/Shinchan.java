/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.familynohara;

/**
 *
 * @author ASUS
 */
//กำหนดว่า class Shinchan เป็นคลาสลูกของ FamilyNoha โดยใช้ extends
public class Shinchan extends FamilyNoha {
    
    public Shinchan(String name,int age,String sex, String job){
         // super ใน class Shinchan มี name,age,sex,job พวกนี้เป็นของ FamilyNoha
        super(name,age,sex,job);
           System.out.println("<Shinchan Create>"); 
    }
    //การOverride Method ข้อมูลชินจัง
    @Override
    public void getDetails() {
        System.out.println("Name :" + " "+ this.name + " "+ ",Age :" + " "+this.age + " "+ "years"  + " " 
        +",Sex :" +" "+ sex  + " " + ",Job :" + " "+ job);
     }
}
