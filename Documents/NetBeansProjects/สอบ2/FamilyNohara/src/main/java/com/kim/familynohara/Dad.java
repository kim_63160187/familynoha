/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.familynohara;

/**
 *
 * @author ASUS
 */

//กำหนดว่า class Dad เป็นคลาสลูกของ FamilyNoha โดยใช้ extends
public class Dad extends  FamilyNoha {
    
    public Dad(String name,int age,String sex, String job){
        // super ใน class Dad มี name,age,sex,job พวกนี้เป็นของ FamilyNoha
        super(name,age,sex,job);
           System.out.println("<Dad Create>"); 
                    
    }
   
    //การOverride Method ข้อมูลพ่อ
    @Override
     public void getDetails() {
        System.out.println("Name :" + " "+ this.name + " "+ ",Age :" + " "+this.age + " "+ "years"  + " " 
        +",Sex :" +" "+ sex  + " " + ",Job :" + " "+ job);
     }
                
            

    
}
