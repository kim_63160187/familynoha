/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.bookstore;

/**
 *
 * @author ASUS
 */
public class TestBook {
    public static void main(String[] args) {
        
        //กำหนดค่าตัวแปรให้เป็นconstructor
                Book book1 = new Book("Demain",320,"Novel");
                book1.showDetails();
                Book book2 = new Book( "ShinChan",45,"Cartoon");
                book2.showDetails();
                Book book3 = new Book("Dinosaurs",450,"Non-fiction");
                book3.showDetails();
                
                //ข้อมูลลูกค้าคนที่1
                ///book1.title = "Demain";
                //book1.cost =  320;
                //book1.type = "Novel";
                
                //ข้อมูลลูกค้าคนที่2
                //book2.title = "ShinChan";
                //book2.cost =  45;
                //book2.type = "Cartoon";
                
                
                //ข้อมูลลูกค้าคนที่3
                //book3.title = "Dinosaurs";
                //book3.cost =  450;
                //book3.type = "Non-fiction";


    }

}
